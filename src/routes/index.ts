import PromiseRouter from 'express-promise-router';
import {Request, Response, Router} from 'express';
import * as crud from '../controllers/crud';

const router: Router = PromiseRouter();

router.get('/getAll/', (req: Request, res: Response): void => {
	crud.getAll()
		.then(result => {
			res.send(result);
		})
		.catch(error => {
			res.status(error.output.statusCode).send(error.output.payload);
		});
});

router.get('/get/:id', (req: Request, res: Response): void => {
	crud.get(req.params.id)
		.then(result => {
			if (result) {
				res.send(result);
			} else {
				res.sendStatus(404);
			}
		})
		.catch(error => {
			res.status(error.output.statusCode).send(error.output.payload);
		});
});

router.post('/create/', (req: Request, res: Response): void => {
	crud.create(req.body)
		.then(result => {
			res.status(201).send(result);
		})
		.catch(error => {
			res.status(error.output.statusCode).send(error.output.payload);
		});
});

router.put('/update/', (req: Request, res: Response): void => {
	crud.update(req.body)
		.then(result => {
			if (result) {
				res.send(result);
			} else {
				res.sendStatus(404);
			}
		})
		.catch(error => {
			res.status(error.output.statusCode).send(error.output.payload);
		});
});

router.delete('/delete/:id', (req: Request, res: Response): void => {
	crud.remove(req.params.id)
		.then(result => {
			res.sendStatus(result ? 204 : 404);
		})
		.catch(error => {
			res.status(error.output.statusCode).send(error.output.payload);
		});
});

export default router;
