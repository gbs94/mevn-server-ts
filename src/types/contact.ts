import {Document} from 'mongoose';

export type TContact = {
	first_name?: string,
	last_name?: string,
	phone_number?: string,
	is_favourite?: boolean
};

export interface IContact extends Document, TContact {
}
