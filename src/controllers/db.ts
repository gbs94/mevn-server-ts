import mongoose from 'mongoose';
import {DB_URL} from '../config';

export function connectDB(): Promise<void> {
	return new Promise((resolve, reject) => {
		mongoose.connect(DB_URL, {useNewUrlParser: true, useUnifiedTopology: true, useCreateIndex: true})
			.then(() => {
				console.log('Mongoose connected successfully!');
				resolve();
			})
			.catch((error) => {
				console.log(`Mongoose connection error: ${error}`);
				reject();
			});
	});
}
