import Boom from '@hapi/boom';
import {Types} from 'mongoose';
import {TContact, IContact} from '../types/contact';
import Contact from '../models/Contact';

const validateId = (id: string): void | never => {
	if (!Types.ObjectId.isValid(id)) throw new Boom.Boom('Invalid id', {statusCode: 400});
};

export const get = async (id: string): Promise<IContact | void> => {
	validateId(id);

	try {
		return <IContact>await Contact.findById(id);
	} catch (error) {
		throw Boom.boomify(error);
	}
};

export const getAll = async (): Promise<Array<IContact> | void> => {
	try {
		return await Contact.find();
	} catch (error) {
		throw Boom.boomify(error);
	}
};

export const create = async (item: TContact): Promise<IContact | never> => {
	try {
		const itemDocument: IContact = new Contact(item);
		return <IContact>await itemDocument.save();
	} catch (error) {
		throw Boom.boomify(error, {statusCode: 400, message: error.MongoError});
	}
};

export const update = async (item: { _id: string } & TContact): Promise<IContact | never> => {
	validateId(item._id);

	try {
		return <IContact>await Contact.findByIdAndUpdate(item._id, item, {new: true, useFindAndModify: false});
	} catch (error) {
		throw Boom.boomify(error);
	}
};

export const remove = async (id: string): Promise<IContact | void> => {
	validateId(id);

	try {
		return <IContact>await Contact.findByIdAndDelete(id);
	} catch (error) {
		throw Boom.boomify(error);
	}
};
