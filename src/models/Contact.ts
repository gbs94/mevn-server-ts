import {model, Schema} from 'mongoose';
import {IContact} from '../types/contact';

const schema: Schema = new Schema({
	first_name: {
		type: String,
		required: true
	},
	last_name: {
		type: String,
		default: ''
	},
	phone_number: {
		type: String,
		required: true,
		unique: true
	},
	is_favourite: {
		type: Boolean,
		default: false
	}
});

export default model<IContact>('Contact', schema);
