# MEVN-server

## Project setup

```
npm install
```

### Compiles and hot-reloads for development

```
npm run dev
```

### Compiles and minifies for production

```
npm run build
```

### Starts compiled server

```
npm start
```

## Steps to start project

1. Clone MEVN-server and MEVN-client to a folder at the same level, for example:
   
   `D:\mevn-task\mevn-server` - server
   
   `D:\mevn-task\mevn-client` - client
   
2. Install dependencies and build client: run `npm i && npm run build` in client's folder.

3. Install dependencies and build server: run `npm i && npm run build` in server's folder.

4. To start server run `npm start`, project will be available at:

```
http://localhost:3000/
```
