import express, {Express} from 'express';
import bodyParser from 'body-parser';
import morgan from 'morgan';
import http from 'http';
import path from 'path';
import appRootPath from 'app-root-path';
import {PORT} from './src/config';
import routes from './src/routes';
import {connectDB} from './src/controllers/db';

const app: Express = express();
const server: http.Server = http.createServer(app);
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());
app.use(morgan('dev'));
app.use('/', express.static(path.join(appRootPath.path, '../mevn-client/dist')));
app.use('/api/', routes);

connectDB()
	.then(() => {
		server.listen(PORT);
		server.on('listening', () => {
			console.log(`Server is listening on port ${PORT}...`);
		});
	})
	.catch(() => console.log('Server start failed'));
